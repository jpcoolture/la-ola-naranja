<?php global $artmag_opt; ?>
<?php if($artmag_opt['instagram-bar-visibility'] == 1){ ?>
<div class="instagram-bar">
	<div class="instagram-bar-title">INSTA<i class="iconmag iconmag-instagram"></i>GRAM</div>
	<div class="instagram-bar-subtitle"><?php echo esc_attr($artmag_opt['instagram-subtitle']); ?></div>
</div>
<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('instagram-footer')) :  ?>
    <div class="no-widget pos-center">
    <?php 
        $allowed_tag = array(
            'a' => array(
                'href' => array(),
            ),
        );
    ?>                     
    <?php echo sprintf(wp_kses( __( 'Please Add Widget from <a href="%s">here</a>', 'artmag' ), $allowed_tag),"wp-admin/widgets.php" ); ?>
    </div>
<?php endif; ?>
<?php } ?>
<div class="footer-container margint60 marginb60 equal-wrapper clearfix">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-sm-4 footone equal">
				<?php if ( is_active_sidebar('footer-1') ) { ?>
                    <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-1')) :  ?>
                            <div class="no-widget">
						    <?php 
						        $allowed_tag = array(
						            'a' => array(
						                'href' => array(),
						            ),
						        );
						    ?>                     
						    <?php echo sprintf(wp_kses( __( 'Please Add Widget from <a href="%s">here</a>', 'artmag' ), $allowed_tag),"wp-admin/widgets.php" ); ?>
						    </div>
                    <?php endif; ?>
                <?php } ?>
			</div>		
			<div class="col-lg-4 col-sm-4 foottwo equal">
				<?php if ( is_active_sidebar('footer-2') ) { ?>
                    <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-2')) :  ?>
                        <div class="no-widget">
					    <?php 
					        $allowed_tag = array(
					            'a' => array(
					                'href' => array(),
					            ),
					        );
					    ?>                     
					    <?php echo sprintf(wp_kses( __( 'Please Add Widget from <a href="%s">here</a>', 'artmag' ), $allowed_tag),"wp-admin/widgets.php" ); ?>
					    </div>
                    <?php endif; ?>
                <?php } ?>
			</div>		
			<div class="col-lg-4 col-sm-4 footthree equal">
				<?php if ( is_active_sidebar('footer-3') ) { ?>
                    <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-3')) :  ?>
                        <div class="no-widget">
					    <?php 
					        $allowed_tag = array(
					            'a' => array(
					                'href' => array(),
					            ),
					        );
					    ?>                     
					    <?php echo sprintf(wp_kses( __( 'Please Add Widget from <a href="%s">here</a>', 'artmag' ), $allowed_tag),"wp-admin/widgets.php" ); ?>
					    </div>
                    <?php endif; ?>
                <?php } ?>
			</div>
		</div>
	</div>
</div>
<div class="bottom-footer pos-center">
	<div class="container">
		<div class="row equal-footer">
			<div class="col-lg-2 col-sm-3 equal">
				<div class="logo pos-center"><!-- Logo Start -->
                    <?php
                    if( empty($artmag_opt['footer-logo']['url']) ) {
                    	$artmag_opt['footer-logo']['url'] = "";
                    }
                    if($artmag_opt['footer-logo']['url'] != "" && $artmag_opt['footer-logo-type'] == "image" ){ ?>
                    	<a class="foot-logo" href="<?php echo esc_url(home_url('/')); ?>"><img alt="logo" src="<?php echo esc_attr($artmag_opt['footer-logo']['url']); ?>"></a>
                    <?php } else { ?>
						<div class="logo-text pos-center">
	                   		<h1><a href="<?php echo esc_url(home_url('/')); ?>"><?php if($artmag_opt['footer-logo-custom-title'] == "") {  bloginfo('name'); } else { echo esc_attr($artmag_opt['footer-logo-custom-title']); }  ?></a></h1>
	                   		<div class="blog-tagline"><p><?php if($artmag_opt['footer-logo-custom-title'] == "") { bloginfo('description'); } else { echo esc_attr($artmag_opt['footer-logo-custom-description']); }  ?></p></div>
                   		</div>
                   	<?php } ?>
				</div>
			</div>
			<div class="col-lg-10 col-sm-9 equal">
				<nav id="footer-menu">
                <?php 
                    if (has_nav_menu('footer-menu')) {
					 	wp_nav_menu(
					 		array(
					 			'theme_location' => 'footer-menu', 
					 			'container' => '', 
					 			'menu_class' => 'nav-collapse', 
					 			'menu_id' => 'navfooter',
					 			'walker' => new description_walker() 
					 		)
					 	);
				 	}
					else {
                    ?><div class="empty-menu"><?php 
					        $allowed_tag = array(
					            'a' => array(
					                'href' => array(),
					            ),
					        );
					    ?>                     
					    <?php echo sprintf(wp_kses( __( 'Please Add Menu from <a href="%s">here</a>', 'artmag' ), $allowed_tag),"wp-admin/nav-menus.php" ); ?>
					    </div>
                    <?php } ?>
            	</nav>
			</div>
		</div>
	</div>
</div>
<a href="#" class="scrollup"><i class="iconmag iconmag-arrow-up"></i><span class="hide-mobile"><?php echo esc_attr__("TOP","artmag"); ?></span></a>
<?php wp_footer(); ?>
</body>
</html>