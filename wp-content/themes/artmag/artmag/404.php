<?php get_header(); ?>
    <div class="error-page container margint30 clearfix pageback">
        <div class="pos-center">
            <div class="error-number"><h1>404</h1></div>
            <h3><?php echo esc_html__("OOOOPSSS! PAGE NOT FOUND!","artmag") ?></h3>
            <div class="read-more button margint20"><a href="<?php echo home_url('/'); ?>"><?php esc_html_e("Homepage","artmag"); ?></a></div>
        </div>
    </div>
<?php get_footer(); ?>

