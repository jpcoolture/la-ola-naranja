<!DOCTYPE html>
<!--[if IE 6]><html class="ie ie6" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 7]><html class="ie ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 9]><html class="ie ie9" <?php language_attributes(); ?>><![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8) | !(IE 9)  ]><!-->
<html <?php language_attributes(); ?>><!--<![endif]-->
<head>
<?php global $artmag_opt; ?>


	<!-- *********	Open Graph Image	*********  -->

	<?php     
		if( is_singular() ) :
        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' );
        $thumb_url = $thumb['0'];
    ?>

  	<meta property="og:image" content="<?php echo $thumb_url; ?>" />

  <?php endif; ?>

	<!-- *********	PAGE TOOLS	*********  -->

	<meta charset="<?php bloginfo('charset'); ?>">

	<!-- *********	MOBILE TOOLS	*********  -->

	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- *********	WORDPRESS TOOLS	*********  -->
	
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<!-- *********	FAVICON TOOLS	*********  -->
	
	<?php if ( ! ( function_exists( 'has_site_icon' ) && has_site_icon() ) ) { ?>
    
	<link rel="shortcut icon" href="<?php echo THEMEROOT."/images/favicon.ico"; ?>" />
	
	<?php }
	else {

    if(empty($artmag_opt['favicon']['url'])){
		$artmag_opt['favicon']['url'] = "";
	}

	if(empty($artmag_opt['ipad_retina_icon']['url'])){
		$artmag_opt['ipad_retina_icon']['url'] = "";
	}

	if(empty($artmag_opt['iphone_icon_retina']['url'])){
		$artmag_opt['iphone_icon_retina']['url'] = "";
	}	

	if(empty($artmag_opt['ipad_icon']['url'])){
		$artmag_opt['ipad_icon']['url'] = "";
	}		

	if(empty($artmag_opt['iphone_icon']['url'])){
		$artmag_opt['iphone_icon']['url'] = "";
	}			

	if($artmag_opt['favicon']['url'] != "") { ?> <link rel="shortcut icon" href="<?php echo esc_attr($artmag_opt['favicon']['url']); ?>" /><?php } 
			else { ?> <link rel="shortcut icon" href="<?php echo THEMEROOT."/images/favicon.ico"; ?>" /> <?php } ?>
	
	<?php if($artmag_opt['ipad_retina_icon']['url'] != "")  { ?> <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo esc_attr($artmag_opt['ipad_retina_icon']['url']); ?>" /> <?php } ?>
	
	<?php if($artmag_opt['iphone_icon_retina']['url'] != "")  { ?> <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo esc_attr($artmag_opt['iphone_icon_retina']['url']); ?>" /> <?php } ?>
	
	<?php if($artmag_opt['ipad_icon']['url'] != "")  { ?> <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo esc_attr($artmag_opt['ipad_icon']['url']); ?>" /> <?php } ?>
	
	<?php if($artmag_opt['iphone_icon']['url'] != "")  { ?> <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo esc_attr($artmag_opt['iphone_icon']['url']); ?>" /> <?php } 

	} ?>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php get_header(); global $artmag_opt; ?>
<?php if($artmag_opt['top-menu'] != false){ ?>
<div class="pre-header clearfix">
	<div class="container">
		<div class="pull-left">
			<nav id="top-menu">
				<?php
		        	if (has_nav_menu('top-menu')) {
					 	wp_nav_menu(
					 		array(
					 			'theme_location' => 'top-menu',
					 			'container' => '',
					 			'menu_class' => 'top-menu',
					 			'menu_id' => 'topmenu',
					 			'walker' => new description_walker() 
					 		)
					 	);
				 	}
				?>
			</nav>
		</div>
		<div class="pull-right top-menu-text">
			<div class="pull-left"><?php echo esc_attr($artmag_opt['top-menu-textly']); ?></div>
			<?php if($artmag_opt['language-bar-top-menu'] == true & class_exists('SitePress')){ ?><div class="pull-left language-wpml-top-menu"><?php language_selector_flags(); ?></div><?php } ?>
		</div>
	</div>
</div>
<?php } ?>
<?php
if(is_home() || is_front_page()){
if($artmag_opt['header-slider'] != false){ ?>
	<div class="mOver-slider">
		<div class="mOver-loading"><div class="mOver-spinner"></div></div>
		<div class="mOver-background">
<?php
$argswp = array(
    'post_type'       =>   "post",
    'posts_per_page'  =>   $artmag_opt['header-slider-number'],
    'post_status'     =>   "publish",
    'category_name'   =>   $artmag_opt['header-slider-category'],
    'ignore_sticky_posts'=> 1,
);
$imgcnt = 1;
$the_query = new WP_Query($argswp);
if ($the_query->have_posts()) :
        while ($the_query->have_posts()) : $the_query->the_post();
    		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "artmag-mover-slider" );
        	$image = $image[0]; 
?>
			<div class="mOver-bg mOver-bg-<?php echo esc_attr($imgcnt); ?>" style="background:url('<?php echo esc_url($image); ?>');"></div>
<?php
$imgcnt++;
endwhile; else :
wp_reset_postdata(); 
endif;
?>
		</div>
		<div class="mOver-wraplist">
			<div class="mOver-mask"></div>
			<div class="container">
				<ul class="mOver-list">
<?php
$argswp = array(
    'post_type'       =>   "post",
    'posts_per_page'  =>   $artmag_opt['header-slider-number'],
    'post_status'     =>   "publish",
    'category_name'   =>   $artmag_opt['header-slider-category'],
    'ignore_sticky_posts'=> 1,
);
$imgcntm = 1;
$the_query = new WP_Query($argswp);
if ($the_query->have_posts()) :
        while ($the_query->have_posts()) : $the_query->the_post(); 
?>
					<li data-video-line="<?php echo esc_attr($imgcntm); ?>">
						<div class="mo-title"><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></div>
						<div class="read-more-mo"><a href="<?php echo get_the_permalink(); ?>"><?php echo esc_attr__("Read More","artmag"); ?></a></div>
					</li>
<?php
$imgcntm++;
endwhile; else : 
wp_reset_postdata(); 
endif;
?>
				</ul>
			</div>
		</div>
	</div>
	<div id="owl-sli-m" class="owl-carousel owl-short mOver-mobile">
<?php
$argswp = array(
    'post_type'       =>   "post",
    'posts_per_page'  =>   $artmag_opt['header-slider-number'],
    'post_status'     =>   "publish",
    'category_name'   =>   $artmag_opt['header-slider-category'],
    'ignore_sticky_posts'=> 1,
);
$the_query = new WP_Query($argswp);
if ($the_query->have_posts()) :
        while ($the_query->have_posts()) : $the_query->the_post(); 
    	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "artmag-five-grid" );
        $image = $image[0]; 
?>
		<div class="item">
        	<img alt="" class="img-responsive" src="<?php echo esc_attr($image); ?>" />
            <div class="mOver-mobile-title">
            	<div class=""><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></div>
            </div>
        </div>
<?php
endwhile; else : 
wp_reset_postdata(); 
endif;
?>
	</div>
<?php }} ?>
	<div class="main-header clearfix"><!-- Main Header Start -->
	    <div class="header-container<?php if($artmag_opt['head-color-type'] == false){ echo ' dark'; } ?>"<?php if($artmag_opt['head-style-type'] == 'image'){ ?> style="background: url('<?php echo esc_attr($artmag_opt['head-image']['url']); ?>');"<?php } ?><?php if($artmag_opt['head-style-type'] == 'colored'){ ?> style="background: <?php echo esc_attr($artmag_opt['head-colored']); ?>;"<?php } ?>>
<?php if($artmag_opt['head-style-type'] == "video"){?>
            <div class="video_sections">
            	<?php
            	$video_url = $artmag_opt['head-video'];
                $youtube = "youtube";
                $vimeo = "vimeo";
                $youtubesec = "youtu.be";
                if (strlen(strstr($video_url,$youtube))>0) {
                  	$videotype = "youtube";
                  	parse_str( parse_url( $video_url, PHP_URL_QUERY ), $videoid );
                  	$videopath = $videoid['v'];  
                }elseif(strlen(strstr($video_url,$youtubesec))>0) {
					$needle = "youtu.be/";
			        $pos = strpos($video_url, $needle);
			        $start = $pos + strlen($needle);
			        $vid_id = substr($video_url, $start, 11);
					$videotype = "youtube";
					$videopath = $vid_id; 
				}elseif(strlen(strstr($video_url,$vimeo))>0){
                  	$videotype = "vimeo";
                  	sscanf(parse_url($video_url, PHP_URL_PATH), '/%d', $videoid);
                  	$videopath = $videoid;
                }else{
                  	$videotype = "mp4";
                  	$videopath = $video_url;
                }
                $randomKey = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
                ?>
                <div class="video-area">
                  <div class="video-section" data-video-height="200">
                    <div class="video-wrapper">
                    <?php if($videotype == "youtube"){ ?>
                      	<div id="youtube-bg<?php echo esc_attr($randomKey); ?>" class="youtube-bg" data-video-url="<?php echo esc_attr($videopath); ?>" data-video-uid="<?php echo esc_attr($randomKey); ?>"></div>
                    <?php }elseif($videotype == "vimeo"){ ?>
                    	<iframe id="player<?php echo esc_attr($randomKey); ?>" class="vimeo-bg" src="http://player.vimeo.com/video/<?php echo esc_attr($videopath); ?>?portrait=0&byline=0&player_id=player<?php echo esc_attr($randomKey); ?>&title=0&badge=0&loop=1&autopause=0&api=1&rel=0&autoplay=1" frameborder="0"></iframe>
                    <?php }else{ $videoPathHtml = substr($videopath, 0, -4); ?>
                    	<video class="mediaElement" preload="false" loop="true" autoPlay="true" poster="<?php echo IMAGES.'/dummy.png'; ?>" muted>
                            <source type="video/mp4" src="<?php echo esc_attr($videoPathHtml); ?>.mp4">
                            <source type="video/webm" src="<?php echo esc_attr($videoPathHtml); ?>.webm">
                        	<source type="video/ogg" src="<?php echo esc_attr($videoPathHtml); ?>.ogg">
                        </video>
                    <?php } ?>
                    	<div class="video-cover" style="background: url('<?php echo esc_url($artmag_opt['head-video-image']['url']); ?>') no-repeat;"></div>
                    </div>
                    <div class="video-content"><div class="video-child">
			<?php } ?>
	    	<div class="container">
	        	<div class="row vertical">

	        		<?php if($artmag_opt['author-info-visibility'] != 1 && $artmag_opt['social-media-visibility'] != 1  ){ ?>
	        		<div class="col-lg-12 col-logo vertical-middle">
						<div class="logo pos-center"><!-- Logo Start -->
		                    <?php

		                    if( empty($artmag_opt['logo']['url']) ) {
		                    	$artmag_opt['logo']['url'] = "";
		                    }
		                    if($artmag_opt['logo']['url'] != "" && $artmag_opt['logo-type'] == "image" ){ ?>
		                    	<a href="<?php echo esc_url(home_url('/')); ?>"><img alt="logo" src="<?php echo esc_attr($artmag_opt['logo']['url']); ?>"></a>
		                    <?php } else { ?>
								<div class="logo-text pos-center">
			                   		<h1><a href="<?php echo esc_url(home_url('/')); ?>"><?php if($artmag_opt['logo-custom-title'] == "") {  bloginfo('name'); } else { echo esc_attr($artmag_opt['logo-custom-title']); }  ?></a></h1>
			                   		<div class="blog-tagline"><p><?php if($artmag_opt['logo-custom-title'] == "") { bloginfo('description'); } else { echo esc_attr($artmag_opt['logo-custom-description']); }  ?></p></div>
		                   		</div>
		                   	<?php } ?>
						</div><!-- Logo Finish -->
	        		</div>
	        		<?php }else { ?>

                	<div class="col-lg-4 col-sm-4 col-user vertical-middle">
                	<?php if($artmag_opt['author-info-visibility'] == 1 ){ ?>
                		<div class="user-info">
                			<div class="user-info-img pull-left"><a href="<?php echo esc_url($artmag_opt['author-link']); ?>"><img alt='' class="img-responsive" src="<?php echo esc_url($artmag_opt['author-image']['url']); ?>"></a></div>
                			<div class="user-info-content pull-left">
                				<h6><a href="<?php echo esc_url($artmag_opt['author-link']); ?>"><?php echo esc_attr($artmag_opt['author-name']); ?></a></h6>
                				<p><?php echo esc_attr($artmag_opt['author-info']); ?> <a href="<?php echo esc_url($artmag_opt['author-link']); ?>"><?php echo esc_attr__("More...","artmag"); ?></a></p>
                			</div>
                		</div>
                	<?php } ?>
                	</div>
                	<div class="col-lg-4 col-sm-4 col-logo vertical-middle">
						<div class="logo pos-center"><!-- Logo Start -->
		                    <?php

		                    if( empty($artmag_opt['logo']['url']) ) {
		                    	$artmag_opt['logo']['url'] = "";
		                    }
		                    if($artmag_opt['logo']['url'] != "" && $artmag_opt['logo-type'] == "image" ){ ?>
		                    	<a href="<?php echo esc_url(home_url('/')); ?>"><img alt="logo" src="<?php echo esc_attr($artmag_opt['logo']['url']); ?>"></a>
		                    <?php } else { ?>
								<div class="logo-text pos-center">
			                   		<h1><a href="<?php echo esc_url(home_url('/')); ?>"><?php if($artmag_opt['logo-custom-title'] == "") {  bloginfo('name'); } else { echo esc_attr($artmag_opt['logo-custom-title']); }  ?></a></h1>
			                   		<div class="blog-tagline"><p><?php if($artmag_opt['logo-custom-title'] == "") { bloginfo('description'); } else { echo esc_attr($artmag_opt['logo-custom-description']); }  ?></p></div>
		                   		</div>
		                   	<?php } ?>
						</div><!-- Logo Finish -->
					</div>
					<div class="col-lg-4 col-sm-4 col-social vertical-middle">
					<?php if($artmag_opt['social-media-visibility'] == 1 ){ ?>
						<div class="social-area pull-right">
							<ul>
								<?php if ($artmag_opt['behance-header'] != "") { ?><li class="behance"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Behance","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['behance-header']); ?>"><i class="iconmag iconmag-behance"></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['bloglovin-header'] != "") { ?><li class="bloglovin"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Bloglovin","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['bloglovin-header']); ?>"><i class="iconmag iconmag-bloglovin"></i></a></li><?php } ?>	
	                            <?php if ($artmag_opt['blogger-header'] != "") { ?><li class="blogger"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Blogger","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['blogger-header']); ?>"><i class="iconmag iconmag-blogger"></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['dribbble-header'] != "") { ?><li class="dribbble"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Dribbble","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['dribbble-header']); ?>"><i class="iconmag iconmag-dribbble"></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['facebook-header'] != "") { ?><li class="facebook"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Facebook","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['facebook-header']); ?>"><i class="iconmag iconmag-facebook"></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['flickr-header'] != "") { ?><li class="flickr"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Flickr","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['flickr-header']); ?>"><i class="iconmag iconmag-flickr"></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['foursquare-header'] != "") { ?><li class="foursquare"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Foursquare","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['foursquare-header']); ?>"><i class="iconmag iconmag-foursquare"></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['google-plus-header'] != "") { ?><li class="google-plus"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Google +","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['google-plus-header']); ?>"><i class="iconmag iconmag-google "></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['instagram-header'] != "") { ?><li class="instagram"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Instagram","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['instagram-header']); ?>"><i class="iconmag iconmag-instagram "></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['linkedin-header'] != "") { ?><li class="linkedin"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Linkedin","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['linkedin-header']); ?>"><i class="iconmag iconmag-linkedin "></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['medium-header'] != "") { ?><li class="medium"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Medium","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['medium-header']); ?>"><i class="iconmag iconmag-medium"></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['pinterest-header'] != "") { ?><li class="pinterest"><a data-toggle="tooltip" data-placement="top"  title="<?php echo esc_html_e("Pinterest","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['pinterest-header']); ?>"><i class="iconmag iconmag-pinterest "></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['rss-header'] != "") { ?><li class="rss"><a data-toggle="tooltip" data-placement="top"  title="<?php echo esc_html_e("RSS Feed","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['rss-header']); ?>"><i class="iconmag iconmag-rss "></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['skype-header'] != "") { ?><li class="skype"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Skype","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['skype-header']); ?>"><i class="iconmag iconmag-skype"></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['telegram-header'] != "") { ?><li class="telegram"><a data-toggle="tooltip" data-placement="top"  title="<?php echo esc_html_e("Telegram","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['telegram-header']); ?>"><i class="iconmag iconmag-telegram "></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['tumblr-header'] != "") { ?><li class="tumblr"><a data-toggle="tooltip" data-placement="top"  title="<?php echo esc_html_e("Tumblr","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['tumblr-header']); ?>"><i class="iconmag iconmag-tumblr "></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['twitter-header'] != "") { ?><li class="twitter"><a data-toggle="tooltip" data-placement="top"  title="<?php echo esc_html_e("Twitter","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['twitter-header']); ?>"><i class="iconmag iconmag-twitter "></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['vimeo-header'] != "") { ?><li class="vimeo"><a data-toggle="tooltip" data-placement="top"  title="<?php echo esc_html_e("Vimeo","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['vimeo-header']); ?>"><i class="iconmag iconmag-vimeo"></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['whatsapp-header'] != "") { ?><li class="whatsapp"><a data-toggle="tooltip" data-placement="top"  title="<?php echo esc_html_e("Whatsapp","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['whatsapp-header']); ?>"><i class="iconmag iconmag-whatsapp"></i></a></li><?php } ?>
	                            <?php if ($artmag_opt['youtube-header'] != "") { ?><li class="youtube"><a data-toggle="tooltip" data-placement="top"  title="<?php echo esc_html_e("Youtube","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['youtube-header']); ?>"><i class="iconmag iconmag-youtube "></i></a></li><?php } ?>
	                            
	                            <?php if ($artmag_opt['custom-site-name-1'] != "") { ?><li class="custom-logo"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_attr($artmag_opt['custom-site-name-1']); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['custom-site-url-1']); ?>"><img alt="<?php echo esc_attr($artmag_opt['custom-site-name-1']); ?>" src="<?php echo esc_attr($artmag_opt['custom-site-logo-1']['url']); ?>"></a></li><?php } ?>
	                            
	                            <?php if ($artmag_opt['custom-site-name-2'] != "") { ?><li class="custom-logo"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_attr($artmag_opt['custom-site-name-2']); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['custom-site-url-2']); ?>"><img alt="<?php echo esc_attr($artmag_opt['custom-site-name-2']); ?>" src="<?php echo esc_attr($artmag_opt['custom-site-logo-2']['url']); ?>"></a></li><?php } ?>
	                            
	                            <?php if ($artmag_opt['custom-site-name-3'] != "") { ?><li class="custom-logo"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_attr($artmag_opt['custom-site-name-3']); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['custom-site-url-3']); ?>"><img alt="<?php echo esc_attr($artmag_opt['custom-site-name-3']); ?>" src="<?php echo esc_attr($artmag_opt['custom-site-logo-3']['url']); ?>"></a></li><?php } ?>	                            
	                            <?php if ($artmag_opt['custom-site-name-4'] != "") { ?><li class="custom-logo"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_attr($artmag_opt['custom-site-name-4']); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['custom-site-url-4']); ?>"><img alt="<?php echo esc_attr($artmag_opt['custom-site-name-4']); ?>" src="<?php echo esc_attr($artmag_opt['custom-site-logo-4']['url']); ?>"></a></li><?php } ?>
	                            
	                            <?php if ($artmag_opt['custom-site-name-5'] != "") { ?><li class="custom-logo"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_attr($artmag_opt['custom-site-name-5']); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['custom-site-url-5']); ?>"><img alt="<?php echo esc_attr($artmag_opt['custom-site-name-5']); ?>" src="<?php echo esc_attr($artmag_opt['custom-site-logo-5']['url']); ?>"></a></li><?php } ?>
	                            
	                            <?php if ($artmag_opt['custom-site-name-6'] != "") { ?><li class="custom-logo"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_attr($artmag_opt['custom-site-name-6']); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['custom-site-url-6']); ?>"><img alt="<?php echo esc_attr($artmag_opt['custom-site-name-6']); ?>" src="<?php echo esc_attr($artmag_opt['custom-site-logo-6']['url']); ?>"></a></li><?php } ?>
								



								<?php if($artmag_opt['search-visibility'] == 1 ){ ?><li class="searchli">
									<a class="search_button isOpenNo" href="#"><i class="iconmag iconmag-search"></i></a>
									<div id="search-wrapper">
			                            <form action="<?php echo home_url('/'); ?>" id="searchform" method="get">
			                                <input type="search" id="s" name="s" class="s-input" placeholder="<?php echo esc_attr__("Write keyword and press enter","artmag"); ?>" required />
			                            </form>
			                        </div>
									</li>
									<?php } ?>
									<?php if($artmag_opt['language-bar-top-menu'] == false & class_exists('SitePress')){ ?><li class="language-wpml"><?php language_selector_flags(); ?></li><?php } ?>
							</ul>
						</div>
						<?php } ?>
					</div>
					<?php } ?>



                </div>
            </div>
<?php if($artmag_opt['head-style-type'] == "video"){?>
                    </div></div>
                  </div>
                </div>
            </div>
<?php } ?>
    	</div>
	</div><!-- Main Header Finish -->

	<div class="mobile-main-header">
		<div class="mobile-pre-header clearfix">
			<div class="pull-left">
				<div class="social-area clearfix">
					<ul>
						<?php if ($artmag_opt['behance-header'] != "") { ?><li class="behance"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Behance","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['behance-header']); ?>"><i class="iconmag iconmag-behance"></i></a></li><?php } ?>
                        <?php if ($artmag_opt['blogger-header'] != "") { ?><li class="blogger"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Clogger","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['blogger-header']); ?>"><i class="iconmag iconmag-blogger"></i></a></li><?php } ?>
                        <?php if ($artmag_opt['dribbble-header'] != "") { ?><li class="dribbble"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Dribbble","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['dribbble-header']); ?>"><i class="iconmag iconmag-dribbble"></i></a></li><?php } ?>
                        <?php if ($artmag_opt['facebook-header'] != "") { ?><li class="facebook"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Facebook","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['facebook-header']); ?>"><i class="iconmag iconmag-facebook"></i></a></li><?php } ?>
                        <?php if ($artmag_opt['flickr-header'] != "") { ?><li class="flickr"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Flickr","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['flickr-header']); ?>"><i class="iconmag iconmag-flickr"></i></a></li><?php } ?>
                        <?php if ($artmag_opt['foursquare-header'] != "") { ?><li class="foursquare"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Foursquare","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['foursquare-header']); ?>"><i class="iconmag iconmag-foursquare"></i></a></li><?php } ?>
                        <?php if ($artmag_opt['google-plus-header'] != "") { ?><li class="google-plus"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Google +","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['google-plus-header']); ?>"><i class="iconmag iconmag-google "></i></a></li><?php } ?>
                        <?php if ($artmag_opt['instagram-header'] != "") { ?><li class="instagram"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Instagram","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['instagram-header']); ?>"><i class="iconmag iconmag-instagram "></i></a></li><?php } ?>
                        <?php if ($artmag_opt['linkedin-header'] != "") { ?><li class="linkedin"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Linkedin","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['linkedin-header']); ?>"><i class="iconmag iconmag-linkedin "></i></a></li><?php } ?>
                        <?php if ($artmag_opt['medium-header'] != "") { ?><li class="medium"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Medium","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['medium-header']); ?>"><i class="iconmag iconmag-medium"></i></a></li><?php } ?>
                        <?php if ($artmag_opt['pinterest-header'] != "") { ?><li class="pinterest"><a data-toggle="tooltip" data-placement="top"  title="<?php echo esc_html_e("Pinterest","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['pinterest-header']); ?>"><i class="iconmag iconmag-pinterest "></i></a></li><?php } ?>
                        <?php if ($artmag_opt['skype-header'] != "") { ?><li class="skype"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_html_e("Skype","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['skype-header']); ?>"><i class="iconmag iconmag-skype"></i></a></li><?php } ?>
                        <?php if ($artmag_opt['tumblr-header'] != "") { ?><li class="tumblr"><a data-toggle="tooltip" data-placement="top"  title="<?php echo esc_html_e("Tumblr","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['tumblr-header']); ?>"><i class="iconmag iconmag-tumblr "></i></a></li><?php } ?>
                        <?php if ($artmag_opt['twitter-header'] != "") { ?><li class="twitter"><a data-toggle="tooltip" data-placement="top"  title="<?php echo esc_html_e("Twitter","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['twitter-header']); ?>"><i class="iconmag iconmag-twitter "></i></a></li><?php } ?>
                        <?php if ($artmag_opt['vimeo-header'] != "") { ?><li class="vimeo"><a data-toggle="tooltip" data-placement="top"  title="<?php echo esc_html_e("Vimeo","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['vimeo-header']); ?>"><i class="iconmag iconmag-vimeo"></i></a></li><?php } ?>
                        <?php if ($artmag_opt['whatsapp-header'] != "") { ?><li class="whatsapp"><a data-toggle="tooltip" data-placement="top"  title="<?php echo esc_html_e("Whatsapp","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['whatsapp-header']); ?>"><i class="iconmag iconmag-whatsapp"></i></a></li><?php } ?>
                        <?php if ($artmag_opt['youtube-header'] != "") { ?><li class="youtube"><a data-toggle="tooltip" data-placement="top"  title="<?php echo esc_html_e("Youtube","artmag"); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['youtube-header']); ?>"><i class="iconmag iconmag-youtube "></i></a></li><?php } ?>
                        <?php if ($artmag_opt['custom-site-name-1'] != "") { ?><li class="custom-logo"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_attr($artmag_opt['custom-site-name-1']); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['custom-site-url-1']); ?>"><img alt="<?php echo esc_attr($artmag_opt['custom-site-name-1']); ?>" src="<?php echo esc_attr($artmag_opt['custom-site-logo-1']['url']); ?>"></a></li><?php } ?>
                        <?php if ($artmag_opt['custom-site-name-2'] != "") { ?><li class="custom-logo"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_attr($artmag_opt['custom-site-name-2']); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['custom-site-url-2']); ?>"><img alt="<?php echo esc_attr($artmag_opt['custom-site-name-2']); ?>" src="<?php echo esc_attr($artmag_opt['custom-site-logo-2']['url']); ?>"></a></li><?php } ?>
                        <?php if ($artmag_opt['custom-site-name-3'] != "") { ?><li class="custom-logo"><a data-toggle="tooltip" data-placement="top" title="<?php echo esc_attr($artmag_opt['custom-site-name-3']); ?>" target="_blank" href="<?php echo esc_attr($artmag_opt['custom-site-url-3']); ?>"><img alt="<?php echo esc_attr($artmag_opt['custom-site-name-3']); ?>" src="<?php echo esc_attr($artmag_opt['custom-site-logo-3']['url']); ?>"></a></li><?php } ?>
						<?php if($artmag_opt['search-visibility'] == 1 ){ ?><li class="searchli">
							<a class="search_button_mobile isOpenNoM" href="#"><i class="iconmag iconmag-search"></i></a>
							<div id="search-wrapper-mobile">
	                            <form action="<?php echo home_url('/'); ?>" id="searchformm" method="get">
	                                <input type="search" name="s" class="s-input" placeholder="<?php echo esc_attr__("Write keyword and press enter","artmag"); ?>" required />
	                            </form>
	                        </div>
							</li>
						<?php } ?>
						<?php if(class_exists('SitePress')){ ?>
						<li class="language-wpml"><?php language_selector_flags(); ?></li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div class="pull-right">
				<?php if($artmag_opt['author-info-visibility'] == 1 ){ ?>
				<div class="user-info">
        			<div class="user-info-img pull-left"><a href="<?php echo esc_url($artmag_opt['author-link']); ?>"><img alt='' class="img-responsive" src="<?php echo esc_url($artmag_opt['author-image']['url']); ?>"></a></div>
        		</div>
        		<?php } ?>
			</div>
		</div>
		<div class="logo pos-center<?php if($artmag_opt['head-color-type'] == false){ echo ' dark'; } ?>"<?php if($artmag_opt['head-style-type'] == 'image'){ ?> style="background: url('<?php echo esc_attr($artmag_opt['head-image']['url']); ?>');"<?php } ?><?php if($artmag_opt['head-style-type'] == 'video'){ ?> style="background: url('<?php echo esc_attr($artmag_opt['head-video-image']['url']); ?>');"<?php } ?><?php if($artmag_opt['head-style-type'] == 'colored'){ ?> style="background: <?php echo esc_attr($artmag_opt['head-colored']); ?>;"<?php } ?>><!-- Logo Start -->
            <?php

            if( empty($artmag_opt['logo']['url']) ) {
            	$artmag_opt['logo']['url'] = "";
            }
            if($artmag_opt['logo']['url'] != "" && $artmag_opt['logo-type'] == "image" ){ ?>
            	<a href="<?php echo esc_url(home_url('/')); ?>"><img alt="logo" src="<?php echo esc_attr($artmag_opt['logo']['url']); ?>"></a>
            <?php } else { ?>

				<div class="logo-text pos-center">
               		<h1><a href="<?php echo esc_url(home_url('/')); ?>"><?php if($artmag_opt['logo-custom-title'] == "") {  bloginfo('name'); } else { echo esc_attr($artmag_opt['logo-custom-title']); }  ?></a></h1>
               		<div class="blog-tagline"><p><?php if($artmag_opt['logo-custom-title'] == "") { bloginfo('description'); } else { echo esc_attr($artmag_opt['logo-custom-description']); }  ?></p></div>
           		</div>

           	<?php } ?>
		</div><!-- Logo Finish -->
		<nav id="mobile-menu">
	        <?php
	        	if (has_nav_menu('mobile-menu')) {
					 	wp_nav_menu(
					 		array(
					 			'theme_location' => 'mobile-menu', 
					 			'container' => '', 
					 			'menu_class' => 'mobile-menu', 
					 			'menu_id' => 'mobilemenu',
					 			'walker' => new description_walker() 
					 		)
					 	);
				 	}
			?>
	    </nav>
	    <div id="mobileMenuWrap"></div>
	</div>
        <?php if ($artmag_opt['navigation-visibility']) { ?><!-- Main Menu Start -->
        <div class="main-menu marginb20 clearfix pos-center">
            <nav id="main-menu">
            <div class="container">
                <?php 
                    if (has_nav_menu('main-menu')) {
					 	wp_nav_menu(
					 		array(
					 			'theme_location' => 'main-menu', 
					 			'container' => '', 
					 			'menu_class' => 'nav-collapse mini-menu', 
					 			'menu_id' => 'navmain',
					 			'walker' => new description_walker() 
					 		)
					 	);
				 	}
					else {
                    ?><div class="empty-menu"><?php 
					        $allowed_tag = array(
					            'a' => array(
					                'href' => array(),
					            ),
					        );
					    ?>                     
					    <?php echo sprintf(wp_kses( __( 'Please Add Menu from <a href="%s">here</a>', 'artmag' ), $allowed_tag),"wp-admin/nav-menus.php" ); ?>
					    </div>
                    <?php } ?>
            </div>
            </nav>
            <div class="hideSubMenuLoading"></div>
        </div>
        <?php }else{ echo "<div class='clearfix marginb30'></div>"; } ?><!-- Main Menu Finish -->