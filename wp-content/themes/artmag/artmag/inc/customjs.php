<?php
function artmag_fm_customjs() {
global $artmag_opt; 
?>
<script type="text/javascript">

	jQuery(document).ready(function(){
		
	<?php if($artmag_opt['header-slider'] != false){ ?>
		"use strict";
		jQuery('.mOver-slider').mOverSlider({
		    columns : <?php echo esc_attr($artmag_opt['header-slider-number']); ?>
		});


	<?php } ?>

		if(jQuery('.rtl').length){
		    var owlDirect = 'rtl';
		}else{
		    var owlDirect = 'ltr';
		}



		jQuery(".owl-short").owlCarousel({
		    direction:owlDirect,
		    slideSpeed : 900,
		    paginationSpeed : 400,
		    singleItem:true,
		    navigation:false,
		    autoPlay : <?php echo esc_attr($artmag_opt['slider-speed']); ?>,
		});

	});
</script>

<?php }
add_action( 'wp_footer', 'artmag_fm_customjs', 20 );
?>